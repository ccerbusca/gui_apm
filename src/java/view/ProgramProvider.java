package view;

import controller.Controller;
import model.ProgramState;
import model.collections.dictionary.Dictionary;
import model.collections.dictionary.Heap;
import model.collections.queue.Queue;
import model.collections.stack.Stack;
import model.expressions.*;
import model.expressions.operations.ArithmeticOperation;
import model.expressions.operations.RelationalOperation;
import model.statements.*;
import model.types.RefType;
import model.types.ValueType;
import model.values.BoolValue;
import model.values.IntValue;
import model.values.StringValue;
import repository.IRepository;
import repository.ProgramStateRepository;

import java.util.List;
import java.util.function.Supplier;

public class ProgramProvider
{
    public static Controller createProgram(IStatement statement, String filename)
    {
        statement.typecheck(new Dictionary<>());
        ProgramState programState = new ProgramState(
                new Stack<>(),
                new Queue<>(),
                new Dictionary<>(),
                new Dictionary<>(),
                new Heap(),
                statement);
        IRepository<ProgramState> repository = new ProgramStateRepository(filename);
        repository.add(programState);
        return new Controller(repository);
    }

    public static Supplier<Controller> supply(IStatement statement, String filename)
    {
        return () -> createProgram(statement, filename);
    }

    public static IStatement firstProgram()
    {
        return new ComposedStatement(
                new VariableDeclarationStatement("v", ValueType.IntType),
                new ComposedStatement(
                        new AssignStatement("v", new ValueExpression(new IntValue(2))),
                        new PrintStatement(new VariableExpression("v"))
                ));
    }

    public static IStatement secondProgram()
    {
        return new ComposedStatement(
                new VariableDeclarationStatement("a", ValueType.IntType),
                new ComposedStatement(
                        new VariableDeclarationStatement("b", ValueType.IntType),
                        new ComposedStatement(
                                new AssignStatement("a",
                                        new ArithmeticExpression(
                                                new ValueExpression(new IntValue(2)),
                                                new ArithmeticExpression(
                                                        new ValueExpression(new IntValue(3)),
                                                        new ValueExpression(new IntValue(5)),
                                                        ArithmeticOperation.MULTIPLICATION),
                                                ArithmeticOperation.ADDITION)),
                                new ComposedStatement(
                                        new AssignStatement("b",
                                                new ArithmeticExpression(
                                                        new VariableExpression("a"),
                                                        new ValueExpression(new IntValue(1)),
                                                        ArithmeticOperation.ADDITION)),
                                        new PrintStatement(new VariableExpression("b"))))));
    }

    public static IStatement thirdProgram()
    {
        return new ComposedStatement(
                new VariableDeclarationStatement("a", ValueType.BoolType),
                new ComposedStatement(
                        new VariableDeclarationStatement("v", ValueType.IntType),
                        new ComposedStatement(
                                new AssignStatement("a", new ValueExpression(new BoolValue(true))),
                                new ComposedStatement(
                                        new IfStatement(
                                                new VariableExpression("a"),
                                                new AssignStatement("v", new ValueExpression(new IntValue(2))),
                                                new AssignStatement("v", new ValueExpression(new IntValue(3)))
                                        ),
                                        new PrintStatement(new VariableExpression("v"))))));
    }

    public static IStatement fourthProgram()
    {
        return new ComposedStatement(
                new VariableDeclarationStatement("varf", ValueType.StringType),
                new ComposedStatement(
                        new AssignStatement("varf", new ValueExpression(new StringValue("test.in"))),
                        new ComposedStatement(
                                new OpenFileRead(new VariableExpression("varf")),
                                new ComposedStatement(
                                        new VariableDeclarationStatement("varc", ValueType.IntType),
                                        new ComposedStatement(
                                                new ReadFileStatement(new VariableExpression("varf"),"varc"),
                                                new ComposedStatement(
                                                        new PrintStatement(new VariableExpression("varc")),
                                                        new ComposedStatement(
                                                                new ReadFileStatement(new VariableExpression("varf"), "varc"),
                                                                new ComposedStatement(
                                                                        new PrintStatement(new VariableExpression("varc")),
                                                                        new CloseReadFileStatement(new VariableExpression("varf"))))))))));
    }

    public static IStatement garbageCollectorExample()
    {
        return new ComposedStatement(
                new VariableDeclarationStatement("v", new RefType(ValueType.IntType)),
                new ComposedStatement(
                        new AllocateStatement("v", new ValueExpression(new IntValue(20))),
                        new ComposedStatement(
                                new VariableDeclarationStatement("a", new RefType(new RefType(ValueType.IntType))),
                                new ComposedStatement(
                                        new AllocateStatement("a", new VariableExpression("v")),
                                        new ComposedStatement(
                                                new AllocateStatement("v", new ValueExpression(new IntValue(30))),
                                                new PrintStatement(new ReadHeapExpression(new ReadHeapExpression(new VariableExpression("a"))))
                                        )
                                )
                        )
                )
        );
    }

    public static IStatement whileExample()
    {
        return new ComposedStatement(
                new VariableDeclarationStatement("v", ValueType.IntType),
                new ComposedStatement(
                        new AssignStatement("v", new ValueExpression(new IntValue(4))),
                        new ComposedStatement(
                                new WhileStatement(
                                        new RelationalExpression(
                                                new VariableExpression("v"),
                                                new ValueExpression(new IntValue(0)),
                                                RelationalOperation.GREATER_THAN),
                                        new ComposedStatement(
                                                new PrintStatement(new VariableExpression("v")),
                                                new AssignStatement(
                                                        "v",
                                                        new ArithmeticExpression(
                                                                new VariableExpression("v"),
                                                                new ValueExpression(new IntValue(1)),
                                                                ArithmeticOperation.SUBTRACTION
                                                        )
                                                )
                                        )
                                ),
                                new PrintStatement(new VariableExpression("v"))
                        )
                )
        );
    }

    private static IStatement aggregateStatements(IStatement... statements)
    {
//        return Stream.of(statements)
//                .reduce(ComposedStatement::new)
//                .orElse(new NopStatement());
        List<IStatement> list = List.of(statements);
        if (statements.length == 0)
            return new NopStatement();
        return aggregateStatements2(list);
    }

    private static IStatement aggregateStatements2(List<IStatement> list)
    {
        if (list.size() == 1)
            return list.get(0);
        return new ComposedStatement(list.get(0), aggregateStatements2(list.subList(1, list.size())));
    }

    public static IStatement forkExample()
    {
        return aggregateStatements(
                new VariableDeclarationStatement("v", ValueType.IntType),
                new VariableDeclarationStatement("a", new RefType(ValueType.IntType)),
                new AssignStatement("v", new ValueExpression(new IntValue(10))),
                new AllocateStatement("a", new ValueExpression(new IntValue(22))),
                new ForkStatement(aggregateStatements(
                        new WriteHeapStatement("a", new ValueExpression(new IntValue(30))),
                        new AssignStatement("v", new ValueExpression(new IntValue(32))),
                        new PrintStatement(new VariableExpression("v")),
                        new PrintStatement(new ReadHeapExpression(new VariableExpression("a"))))),
                new PrintStatement(new VariableExpression("v")),
                new PrintStatement(new ReadHeapExpression(new VariableExpression("a")))
        );
    }

    public static IStatement badProgram()
    {
        return aggregateStatements(
            new VariableDeclarationStatement("a", ValueType.IntType),
            new AssignStatement("a", new ValueExpression(new StringValue("bad")))
        );
    }

    public static IStatement vladExample()
    {
        IStatement statement = aggregateStatements(new AssignStatement("x",new ValueExpression(new IntValue(15))),
                new ForkStatement(
                        new ComposedStatement(
                                new AssignStatement("x",new ValueExpression(new IntValue(20))),
                                new PrintStatement(new VariableExpression("x")))),
                new NopStatement(),
                new PrintStatement(new VariableExpression("x")));
        return aggregateStatements(
                new VariableDeclarationStatement("x", ValueType.IntType),
                new AssignStatement("x",new ValueExpression(new IntValue(10))),
                new ForkStatement(statement),
                new NopStatement(),
                new NopStatement()
        );
    }
}
