package view.console;

import view.console.commands.ExitCommand;
import view.console.commands.RunExample;

import static view.ProgramProvider.*;

public class Interpreter
{
    public static void main(String[] args) {
       TextMenu textMenu = new TextMenu();

       textMenu.addCommand(new ExitCommand("exit", "exit"));
       textMenu.addCommand(new RunExample("1", "first program", supply(firstProgram(), "log1.txt")));
       textMenu.addCommand(new RunExample("2", "second program", supply(secondProgram(), "log2.txt")));
       textMenu.addCommand(new RunExample("3", "third program", supply(thirdProgram(), "log3.txt")));
       textMenu.addCommand(new RunExample("4", "fourth program", supply(fourthProgram(), "log4.txt")));
       textMenu.addCommand(new RunExample(
               "5", "garbage collector example", supply(garbageCollectorExample(), "garbageCollector.txt")));
       textMenu.addCommand(new RunExample("6", "while example", supply(whileExample(), "whileExample.txt")));
       textMenu.addCommand(new RunExample("7", "fork example", supply(forkExample(), "forkExample.txt")));
       textMenu.addCommand(new RunExample("8", "bad example", supply(badProgram(), "badExample.txt")));
       textMenu.addCommand(new RunExample("9", "vlad", supply(vladExample(), "vlad.txt")));
       textMenu.show();
    }

}
