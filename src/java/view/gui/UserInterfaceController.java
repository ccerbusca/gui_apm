package view.gui;


import controller.Controller;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import model.ProgramState;
import model.statements.IStatement;
import model.values.IValue;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserInterfaceController
{
    @FXML Button button;
    @FXML VBox mainVBox;
    @FXML ListView<ProgramStateWrapper> programStates;
    @FXML ListView<String> out;
    @FXML Label nrOfPrgStates;

    @FXML TableView<Map.Entry<Integer, IValue>> heapTable;
    @FXML TableColumn<Map.Entry<Integer, IValue>, Integer> addresses;
    @FXML TableColumn<Map.Entry<Integer, IValue>, IValue> heapValues;

    @FXML TableView<Map.Entry<String, IValue>> symTable;
    @FXML TableColumn<Map.Entry<String, IValue>, String> varNames;
    @FXML TableColumn<Map.Entry<String, IValue>, IValue> values;

    @FXML ListView<IStatement> executionStack;

    private Controller controller;

    public UserInterfaceController(Controller controller)
    {
        this.controller = controller;
    }

    @FXML
    public void initialize()
    {
        AnchorPane.setTopAnchor(mainVBox, 10d);
        AnchorPane.setBottomAnchor(mainVBox, 10d);
        AnchorPane.setLeftAnchor(mainVBox, 10d);
        AnchorPane.setRightAnchor(mainVBox, 10d);

        programStates.getSelectionModel().selectedItemProperty().addListener((observableValue, old, newValue) -> {
            if (programStates.getSelectionModel().getSelectedItem() != null)
            {
                populateSymTable(newValue.getProgramState());
                populateExecutionStack(newValue.getProgramState());
            }
        });

        button.setOnAction(e -> {
            if (controller.getProgramStates().size() > 0)
            {
                if (controller.oneStep())
                {

                    int selectedIndex = programStates.getSelectionModel().getSelectedIndex();
                    int size = programStates.getItems().size();
                    //programStates.getSelectionModel().clearSelection();
                    populate(true);
                    symTable.getItems().clear();
                    executionStack.getItems().clear();
                    if (size <= programStates.getItems().size())
                        programStates.getSelectionModel().select(selectedIndex);
                }
                else
                {
                    alertNoPrgStates();
                }

            }
            else
            {
                alertNoPrgStates();
            }
        });
        populate(false);
    }

    void alertNoPrgStates()
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Invalid action");
        alert.setHeaderText("There are no more Program States!");
        alert.showAndWait();
    }

    void populate(boolean updateProgramStates)
    {
        populateHeapTable();
        populateOut();
        if (updateProgramStates)
            controller.updateRemovedProgramStates();
        populateProgramStates();
        setNrOfPrgStates();
    }

    void populateOut()
    {
        out.setItems(FXCollections.observableArrayList(
                        controller.getProgramStates().get(0).getOut().getAll().stream()
                                .map(Objects::toString).collect(Collectors.toList())));
    }

    void populateHeapTable()
    {
        heapTable.setItems(FXCollections.observableArrayList(controller.getProgramStates().get(0).getHeap().getEntries()));
        addresses.setCellValueFactory(addressCell ->
                new ReadOnlyObjectWrapper<>(addressCell.getValue().getKey()));
        heapValues.setCellValueFactory(valueCell ->
                new ReadOnlyObjectWrapper<>(valueCell.getValue().getValue()));
    }

    void populateExecutionStack(ProgramState programState)
    {
        executionStack.setItems(FXCollections.observableArrayList(programState.getExecutionStack().getAll()));
    }

    void populateSymTable(ProgramState programState)
    {
        symTable.setItems(FXCollections.observableArrayList(programState.getSymTable().getContent().entrySet()));
        varNames.setCellValueFactory(entryStringCellDataFeatures ->
                new ReadOnlyObjectWrapper<>(entryStringCellDataFeatures.getValue().getKey()));
        values.setCellValueFactory(entryIValueCellDataFeatures ->
                new ReadOnlyObjectWrapper<>(entryIValueCellDataFeatures.getValue().getValue()));
    }

    void setNrOfPrgStates()
    {
        nrOfPrgStates.setText("Number of program states: " + controller.getProgramStates().size());
    }

    void populateProgramStates()
    {
        ObservableList<ProgramStateWrapper> programStateWrappers = FXCollections.observableArrayList(
                controller.getProgramStates().stream()
                .map(ProgramStateWrapper::new)
                .collect(Collectors.toList()));
        programStates.setItems(programStateWrappers);
    }
}

class ProgramStateWrapper
{
    private ProgramState programState;
    private String id;

    ProgramStateWrapper(ProgramState programState)
    {
        this.programState = programState;
        this.id = String.valueOf(programState.getId());
    }

    public ProgramState getProgramState()
    {
        return programState;
    }

    public void setProgramState(ProgramState programState)
    {
        this.programState = programState;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return id;
    }
}
