package view.gui;

import controller.Controller;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.exceptions.SomeException;
import model.statements.IStatement;
import view.ProgramProvider;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Start extends Application
{

    void populatePrograms(ListView<IStatement> programs)
    {
        ObservableList<IStatement> programList = FXCollections.observableArrayList(
                ProgramProvider.firstProgram(),
                ProgramProvider.secondProgram(),
                ProgramProvider.thirdProgram(),
                ProgramProvider.fourthProgram(),
                ProgramProvider.garbageCollectorExample(),
                ProgramProvider.whileExample(),
                ProgramProvider.forkExample(),
                ProgramProvider.badProgram(),
                ProgramProvider.vladExample()
        );
        programs.setItems(programList);
        programs.getSelectionModel().select(0);
    }

    private void createProgramChooser(Stage stage)
    {
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefWidth(700);

        VBox vBox1 = new VBox();
        AnchorPane.setTopAnchor(vBox1, 10d);
        AnchorPane.setBottomAnchor(vBox1, 10d);
        AnchorPane.setLeftAnchor(vBox1, 10d);
        AnchorPane.setRightAnchor(vBox1, 10d);

        ListView<IStatement> programs= new ListView<>();
        populatePrograms(programs);

        VBox vBox2 = new VBox();
        vBox2.setAlignment(Pos.CENTER);
        VBox.setMargin(vBox2, new Insets(10, 0, 0, 0));
        Button button = new Button();
        button.setText("Select");
        button.setOnAction(e ->
                {
                    try
                    {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("UserInterfaceController.fxml"));
                        try
                        {
                            loader.setController(new UserInterfaceController(
                                            ProgramProvider.createProgram(programs.getSelectionModel().getSelectedItem(),
                                                    "log_" + LocalDateTime.now().format(
                                                            DateTimeFormatter.BASIC_ISO_DATE) + ".txt")
                                    )
                            );
                            Scene scene = new Scene(loader.load());
                            scene.getStylesheets().add("UserInterfaceController.css");
                            stage.setTitle("Interpreter GUI");
                            stage.setScene(scene);
                        }
                        catch (SomeException exception)
                        {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Exception");
                            alert.setHeaderText(exception.getMessage());
                            alert.showAndWait();
                        }
                    }
                    catch (IOException ex)
                    {
                        ex.printStackTrace();
                    }
                }
        );

        vBox2.getChildren().add(button);
        vBox1.getChildren().addAll(programs, vBox2);
        anchorPane.getChildren().add(vBox1);
        Scene scene = new Scene(anchorPane);
        scene.getStylesheets().add("UserInterfaceController.css");
        stage.setScene(scene);
        stage.setResizable(false);
    }

    @Override
    public void start(Stage stage)
    {
        createProgramChooser(stage);
        stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
        stage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
