package controller;

import model.ProgramState;
import model.collections.dictionary.IHeap;
import model.types.RefType;
import model.values.IValue;
import model.values.RefValue;
import repository.IRepository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {

    private IRepository<ProgramState> programRepository;
    private ExecutorService executor;
    private String initialProgram;

    public Controller(IRepository<ProgramState> repository)
    {
        this.programRepository = repository;
        this.initialProgram = repository.getAll().get(0).getExecutionStack().toString();
        executor = Executors.newFixedThreadPool(4);
    }

    public List<ProgramState> getProgramStates()
    {
        return programRepository.getAll();
    }

    private Map<Integer, IValue> unsafeGarbageCollector(IHeap<IValue> heap, List<Integer> addresses)
    {
        return heap.getEntries().stream()
                .filter(entry -> addresses.contains(entry.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private void applyGarbageCollector(List<ProgramState> programStates)
    {
        IHeap<IValue> heap = programStates.get(0).getHeap();
        Map<Integer, IValue> map = unsafeGarbageCollector(heap, getAddressesFromAllProgramStates(programStates, heap));
        programStates.forEach(state -> state.getHeap().setHeap(map));
    }

    private Integer getInnerAddress(IHeap<IValue> heap, RefValue value)
    {
        RefType type = (RefType) value.getType();
        if (value.getAddress() == 0)
            return 0;
        if (type.getInner() instanceof RefType)
            return getInnerAddress(heap, (RefValue) heap.lookup(value.getAddress()));
        return value.getAddress();
    }

    private List<Integer> getAddressesFromSymTable(Collection<RefValue> values, IHeap<IValue> heap)
    {
        return Stream.of(
                    values.stream()
                        .map(v -> getInnerAddress(heap, v)),
                    values.stream()
                        .map(RefValue::getAddress))
                .flatMap(Function.identity())
                .distinct()
                .collect(Collectors.toList());
    }

    private List<Integer> getAddressesFromAllProgramStates(List<ProgramState> programStates, IHeap<IValue> heap)
    {
        List<RefValue> collect = programStates.stream()
                .map(state -> state.getSymTable().getContent().values().stream())
                .flatMap(Function.identity())
                .filter(value -> value instanceof RefValue)
                .map(value -> (RefValue) value)
                .collect(Collectors.toList());
        return getAddressesFromSymTable(collect, heap);
    }

    private List<ProgramState> removeCompletedProgramStates(List<ProgramState> programStates)
    {
        return programStates.stream()
                .filter(ProgramState::isNotCompleted)
                .collect(Collectors.toList());
    }

    public void oneStepForAllProgramStates(List<ProgramState> programStates)
    {
        programStates.forEach(programRepository::logPrgStateExec);
        List<Callable<ProgramState>> collect = programStates.stream()
                .map(p -> (Callable<ProgramState>) (p::oneStep))
                .collect(Collectors.toList());
        try
        {
            List<ProgramState> newCollect = executor.invokeAll(collect).stream()
                    .map(future -> {
                        try { return future.get(); }
                        catch (ExecutionException | InterruptedException ignored) { return null; }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            programStates.addAll(newCollect);
        }
        catch (InterruptedException ignored) { }
        programStates.forEach(programRepository::logPrgStateExec);
        programRepository.setProgramList(programStates);
    }

    public void allStep()
    {
        List<ProgramState> programStates = removeCompletedProgramStates(programRepository.getAll());
        while (!programStates.isEmpty())
        {
            applyGarbageCollector(programStates);
            oneStepForAllProgramStates(programStates);
            programStates = removeCompletedProgramStates(programRepository.getAll());
        }
        executor.shutdownNow();
        programRepository.setProgramList(programStates);
    }

    public boolean oneStep()
    {
        List<ProgramState> programStates = removeCompletedProgramStates(programRepository.getAll());
        if (programStates.isEmpty())
        {
            programRepository.setProgramList(programStates);
            executor.shutdownNow();
            return false;
        }
        applyGarbageCollector(programStates);
        oneStepForAllProgramStates(programStates);
        programRepository.setProgramList(programStates);
        return true;
    }

    public void updateRemovedProgramStates()
    {
        programRepository.setProgramList(removeCompletedProgramStates(programRepository.getAll()));
    }

    @Override
    public String toString()
    {
        return initialProgram;
    }
}
