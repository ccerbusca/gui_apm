package model;

import model.collections.dictionary.IDictionary;
import model.collections.dictionary.IHeap;
import model.collections.queue.IQueue;
import model.collections.stack.IStack;
import model.exceptions.SomeException;
import model.statements.IStatement;
import model.values.IValue;
import model.values.StringValue;

import java.io.BufferedReader;

public class ProgramState {

    private static int nextID = 0;

    private IStack<IStatement> executionStack;
    private IQueue<IValue> out;
    private IDictionary<String, IValue> symTable;
    private IDictionary<StringValue, BufferedReader> fileTable;
    private IHeap<IValue> heap;
    private int id;

    public ProgramState(IStack<IStatement> executionStack,
                        IQueue<IValue> out,
                        IDictionary<String, IValue> symTable,
                        IDictionary<StringValue, BufferedReader> fileTable,
                        IHeap<IValue> heap,
                        IStatement program)
    {
        setID();
        this.executionStack = executionStack;
        this.out = out;
        this.symTable = symTable;
        this.fileTable = fileTable;
        this.heap = heap;
        executionStack.push(program);
    }

    private synchronized void setID()
    {
        id = nextID++;
    }

    public IStack<IStatement> getExecutionStack() {
        return executionStack;
    }

    public void setExecutionStack(IStack<IStatement> executionStack) {
        this.executionStack = executionStack;
    }

    public IQueue<IValue> getOut() {
        return out;
    }

    public void setOut(IQueue<IValue> out) {
        this.out = out;
    }

    public IDictionary<String, IValue> getSymTable() {
        return symTable;
    }

    public void setSymTable(IDictionary<String, IValue> symTable) {
        this.symTable = symTable;
    }

    public IDictionary<StringValue, BufferedReader> getFileTable()
    {
        return fileTable;
    }

    public void setFileTable(IDictionary<StringValue, BufferedReader> fileTable)
    {
        this.fileTable = fileTable;
    }

    public IHeap<IValue> getHeap()
    {
        return heap;
    }

    public void setHeap(IHeap<IValue> heap)
    {
        this.heap = heap;
    }

    public Boolean isNotCompleted()
    {
        return !executionStack.isEmpty();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public ProgramState oneStep() throws SomeException
    {
        if (executionStack.isEmpty())
            throw new SomeException("Stack is empty");
        IStatement statement = executionStack.pop();
        return statement.execute(this);
    }

    @Override
    public String toString()
    {
        return "Id:" + id + "\n" +
                "ExecutionStack:\n" + executionStack + "\n" +
                "SymTable:\n" + symTable + "\n" +
                "FileTable:\n" + fileTable + "\n" +
                "Heap:\n" + heap + "\n" +
                "Out:\n" + out + "\n";
    }
}
