package model.types;

import model.values.*;

public enum ValueType implements Type
{
    IntType("int", new IntValue(0)),
    BoolType("bool", new BoolValue(false)),
    StringType("string", new StringValue(""));

    private String message;
    private IValue defaultValue;
    ValueType(String message, IValue defaultValue)
    {
        this.message = message;
        this.defaultValue = defaultValue;
    }

    @Override
    public IValue getDefaultValue()
    {
        return defaultValue;
    }

    @Override
    public String toString()
    {
        return message;
    }
}
