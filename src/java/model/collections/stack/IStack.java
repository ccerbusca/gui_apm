package model.collections.stack;

import java.util.Collection;

public interface IStack<T> {
    T pop();
    void push(T v);
    boolean isEmpty();
    Collection<T> getAll();
}
