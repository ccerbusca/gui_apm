package model.collections.stack;

import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.Collectors;

public class Stack<T> implements IStack<T> {

    private Deque<T> stack = new ConcurrentLinkedDeque<>();

    @Override
    public T pop() {
        return stack.pop();
    }

    @Override
    public void push(T v) {
        stack.push(v);
    }

    @Override
    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    @Override
    public Collection<T> getAll()
    {
        return stack;
    }

    @Override
    public String toString()
    {
        return stack.stream()
                .map(Objects::toString)
                .collect(Collectors.joining("\n"));
    }
}
