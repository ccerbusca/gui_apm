package model.collections.dictionary;

import model.exceptions.SomeException;
import model.values.IValue;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Heap implements IHeap<IValue>
{
    private Map<Integer, IValue> heap;
    private AtomicInteger seed;

    public Heap()
    {
        seed = new AtomicInteger(1);
        heap = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized Integer put(IValue value)
    {
        heap.put(seed.get(), value);
        return seed.getAndIncrement();
    }

    @Override
    public IValue lookup(Integer key) throws SomeException
    {
        if (!heap.containsKey(key))
            throw new SomeException("Address not allocated");
        return heap.get(key);
    }

    public boolean contains(Integer key)
    {
        return heap.containsKey(key);
    }

    @Override
    public void replace(Integer key, IValue value)
    {
        heap.put(key, value);
    }

    @Override
    public void setHeap(Map<Integer, IValue> heap)
    {
        this.heap = heap;
    }

    @Override
    public Set<Map.Entry<Integer, IValue>> getEntries()
    {
        return heap.entrySet();
    }

    @Override
    public String toString()
    {
        return heap.entrySet().stream()
                .map(pair -> pair.getKey() + " -> " + pair.getValue())
                .collect(Collectors.joining("\n"));
    }
}
