package model.collections.dictionary;

import model.deepcopy.DeepCopyable;

import java.util.Map;
import java.util.Set;

public interface IDictionary<K, V> extends DeepCopyable<IDictionary<K, V>>
{
    V lookup(K key);
    void put(K key, V value);
    boolean exists(K key);
    void delete(K key);
    Map<K, V> getContent();
}
