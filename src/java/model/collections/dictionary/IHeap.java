package model.collections.dictionary;

import model.exceptions.SomeException;
import model.values.IValue;

import java.util.Map;
import java.util.Set;

public interface IHeap<V>
{
    Integer put(V value);
    IValue lookup(Integer key) throws SomeException;
    boolean contains(Integer key);
    void replace(Integer key, IValue value);
    void setHeap(Map<Integer, V> heap);
    Set<Map.Entry<Integer, V>> getEntries();
}
