package model.deepcopy;

public interface DeepCopyable<T>
{
    T deepCopy();
}
