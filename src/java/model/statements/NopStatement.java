package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.types.Type;

public class NopStatement implements IStatement {
    @Override
    public ProgramState execute(ProgramState state)
    {
        return null;
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        return typeEnv;
    }

    @Override
    public String toString()
    {
        return "NopStatement{}";
    }
}
