package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.IExpression;
import model.types.Type;
import model.types.ValueType;
import model.values.IValue;

public class WhileStatement implements IStatement
{
    private IExpression expression;
    private IStatement statement;

    public WhileStatement(IExpression expression, IStatement statement)
    {
        this.expression = expression;
        this.statement = statement;
    }

    @Override
    public ProgramState execute(ProgramState state)
    {
        IValue eval = expression.eval(state);
        if (eval.getType() == ValueType.BoolType)
        {
            if (eval.getValue().equals(Boolean.TRUE))
            {
                state.getExecutionStack().push(this);
                state.getExecutionStack().push(statement);
            }
        }
        else
            throw new SomeException("Expression does not evaluate to Bool Type");
        return null;
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        Type type = expression.typecheck(typeEnv);
        if (!type.equals(ValueType.BoolType))
            throw new SomeException("while: condition is not evaluated to bool");
        statement.typecheck(typeEnv.deepCopy());
        return typeEnv;
    }

    @Override
    public String toString()
    {
        return "while(" + expression + ") {"  + statement + '}';
    }
}
