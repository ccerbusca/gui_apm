package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.IExpression;
import model.types.RefType;
import model.types.Type;
import model.values.IValue;
import model.values.RefValue;

public class AllocateStatement implements IStatement
{
    private String varName;
    private IExpression expression;

    public AllocateStatement(String varName, IExpression expression)
    {
        this.varName = varName;
        this.expression = expression;
    }


    public ProgramState execute(ProgramState state)
    {
        if (state.getSymTable().exists(varName))
        {
            IValue lookup = state.getSymTable().lookup(varName);
            if (lookup.getType() instanceof RefType)
            {
                RefValue refValue = (RefValue) lookup;
                IValue value = expression.eval(state);
                if (value.getType().equals(((RefType)refValue.getType()).getInner()))
                {
                    Integer address = state.getHeap().put(value);
                    state.getSymTable().put(varName, new RefValue(address, value.getType()));
                }
                else
                    throw new SomeException(String.format("Variable %s is not a reference to %s", varName, lookup.getType()));
            }
            else
                throw new SomeException("Variable type is not a reference one");
        }
        else
            throw new SomeException(String.format("Variable %s does not exist.", varName));

        return null;
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        Type lookup = typeEnv.lookup(varName);
        Type typecheck = expression.typecheck(typeEnv);
        if (lookup.equals(new RefType(typecheck)))
            return typeEnv;
        else
            throw new SomeException("Allocate: right hand side and left hand side have different types");
    }

    @Override
    public String toString()
    {
        return varName + " = alloc(" + expression + ')';
    }
}
