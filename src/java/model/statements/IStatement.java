package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.types.Type;

public interface IStatement {
    ProgramState execute(ProgramState state);
    IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv);
}
