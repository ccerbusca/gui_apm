package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.expressions.IExpression;
import model.types.Type;

public class PrintStatement implements IStatement {
    private IExpression expression;

    public PrintStatement(IExpression expression) {
        this.expression = expression;
    }

    @Override
    public ProgramState execute(ProgramState state) {
        state.getOut().enqueue(expression.eval(state));
        return null;
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        expression.typecheck(typeEnv);
        return typeEnv;
    }

    @Override
    public String toString()
    {
        return "print(" + expression + ')';
    }
}
