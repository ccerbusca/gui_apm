package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.collections.stack.IStack;
import model.types.Type;

public class ComposedStatement implements IStatement {
    private IStatement first;
    private IStatement second;

    public ComposedStatement(IStatement first, IStatement second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public ProgramState execute(ProgramState state) {
        IStack<IStatement> executionStack = state.getExecutionStack();
        executionStack.push(second);
        executionStack.push(first);
        return null;
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        return second.typecheck(first.typecheck(typeEnv));
    }

    @Override
    public String toString()
    {
        return "("+ first.toString() + ";" + second.toString() + ")";
    }
}
