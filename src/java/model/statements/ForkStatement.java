package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.collections.stack.Stack;
import model.types.Type;

public class ForkStatement implements IStatement
{
    private IStatement statement;

    public ForkStatement(IStatement statement)
    {
        this.statement = statement;
    }

    @Override
    public ProgramState execute(ProgramState state)
    {
        return new ProgramState(
                new Stack<>(),
                state.getOut(),
                state.getSymTable().deepCopy(),
                state.getFileTable(),
                state.getHeap(),
                statement
        );
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        return statement.typecheck(typeEnv.deepCopy());
    }

    @Override
    public String toString()
    {
        return "fork(" + statement.toString() + ")";
    }
}
