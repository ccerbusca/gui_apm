package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.IExpression;
import model.types.Type;
import model.types.ValueType;
import model.values.BoolValue;
import model.values.IValue;

public class IfStatement implements IStatement {
    private IExpression expression;
    private IStatement thenS;
    private IStatement elseS;

    public IfStatement(IExpression expression, IStatement thenS, IStatement elseS)
    {
        this.expression = expression;
        this.thenS = thenS;
        this.elseS = elseS;
    }

    @Override
    public ProgramState execute(ProgramState state) throws SomeException
    {
        IValue eval = expression.eval(state);
        if (eval.getType() == ValueType.BoolType)
        {
            BoolValue val = (BoolValue)eval;
            if (val.getValue())
            {
                return thenS.execute(state);
            }
            else
                return elseS.execute(state);
        }
        else
            throw new SomeException("If expression does not evaluate to boolean");
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        Type typecheck = expression.typecheck(typeEnv);
        if (!typecheck.equals(ValueType.BoolType))
            throw new SomeException("If: condition expression does not have bool type");
        thenS.typecheck(typeEnv.deepCopy());
        elseS.typecheck(typeEnv.deepCopy());
        return typeEnv;
    }

    @Override
    public String toString()
    {
        return "{if(" + expression + ") then " + thenS + " else " + elseS + '}';
    }
}
