package model.statements;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.IExpression;
import model.types.RefType;
import model.types.Type;
import model.values.IValue;
import model.values.RefValue;

import java.sql.Ref;

public class WriteHeapStatement implements IStatement
{
    private String varName;
    private IExpression expression;

    public WriteHeapStatement(String varName, IExpression expression)
    {
        this.varName = varName;
        this.expression = expression;
    }

    @Override
    public ProgramState execute(ProgramState state)
    {
        if (state.getSymTable().exists(varName))
        {
            IValue value = state.getSymTable().lookup(varName);
            if (value.getType() instanceof RefType)
            {
                RefValue refValue = (RefValue) value;
                if (state.getHeap().contains(refValue.getAddress()))
                {
                    RefType refType = (RefType) refValue.getType();
                    IValue eval = expression.eval(state);
                    if (refType.getInner().equals(eval.getType()))
                    {
                        state.getHeap().replace(refValue.getAddress(), eval);
                    }
                }
                else
                    throw new SomeException("Address does not exist");
            }
            else
                throw new SomeException("Variable not reference type");
        }
        else
            throw new SomeException(String.format("Variable %s does not exist", varName));
        return null;
    }

    @Override
    public IDictionary<String, Type> typecheck(IDictionary<String, Type> typeEnv)
    {
        Type lookup = typeEnv.lookup(varName);
        Type typecheck = expression.typecheck(typeEnv);
        if (lookup.equals(new RefType(typecheck)))
            return typeEnv;
        else
            throw new SomeException("writeHeap: variable is not a ref type with" +
                    "an inner type equal to the expressions evaluation type");
    }

    @Override
    public String toString()
    {
        return "*" + varName + " = " + expression;
    }
}
