package model.values;

import model.types.Type;

public interface IValue
{
    Type getType();
    Object getValue();
}
