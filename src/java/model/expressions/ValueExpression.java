package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.types.Type;
import model.values.IValue;

public class ValueExpression implements IExpression {
    private IValue value;

    public ValueExpression(IValue value)
    {
        this.value = value;
    }

    @Override
    public IValue eval(ProgramState state) throws SomeException
    {
        return value;
    }

    @Override
    public Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException
    {
        return value.getType();
    }

    @Override
    public String toString()
    {
        return value.toString();
    }
}
