package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.types.Type;
import model.values.IValue;

public class VariableExpression implements IExpression {
    private String id;

    public VariableExpression(String id)
    {
        this.id = id;
    }

    @Override
    public IValue eval(ProgramState state) throws SomeException
    {
        return state.getSymTable().lookup(id);
    }

    @Override
    public Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException
    {
        return typeEnv.lookup(id);
    }

    @Override
    public String toString()
    {
        return id;
    }
}
