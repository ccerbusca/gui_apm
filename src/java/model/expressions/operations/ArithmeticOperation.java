package model.expressions.operations;

public enum ArithmeticOperation {
    ADDITION("+"),
    SUBTRACTION("-"),
    DIVISION("/"),
    MULTIPLICATION("*");

    ArithmeticOperation(String math)
    {
        this.math = math;
    }

    private String math;

    @Override
    public String toString()
    {
        return math;
    }
}
