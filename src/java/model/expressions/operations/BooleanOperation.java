package model.expressions.operations;

public enum BooleanOperation
{
    AND("&&"),
    OR("||"),
    XOR("^");

    BooleanOperation(String bool)
    {
        this.bool = bool;
    }

    private String bool;

    @Override
    public String toString()
    {
        return bool;
    }
}
