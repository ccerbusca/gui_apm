package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.types.RefType;
import model.types.Type;
import model.values.IValue;
import model.values.RefValue;

public class ReadHeapExpression implements IExpression
{
    private IExpression expression;

    public ReadHeapExpression(IExpression expression)
    {
        this.expression = expression;
    }

    @Override
    public IValue eval(ProgramState state) throws SomeException
    {
        IValue value = expression.eval(state);
        if (value.getType() instanceof RefType)
        {
            RefValue refValue = (RefValue) value;
            if (state.getHeap().contains(refValue.getAddress()))
            {
                return state.getHeap().lookup(refValue.getAddress());
            }
            else
                throw new SomeException("Address does not exist");
        }
        else
            throw new SomeException("Not a reference.");
    }

    @Override
    public Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException
    {
        Type type = expression.typecheck(typeEnv);
        if (type instanceof RefType)
        {
            RefType ref = (RefType) type;
            return ref.getInner();
        }
        else
            throw new SomeException("readHeap argument is not a ref type");
    }

    @Override
    public String toString()
    {
        return "ReadHeapExpression{" + expression + '}';
    }
}
