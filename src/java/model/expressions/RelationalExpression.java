package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.operations.RelationalOperation;
import model.types.Type;
import model.types.ValueType;
import model.values.BoolValue;
import model.values.IValue;
import model.values.IntValue;

public class RelationalExpression implements IExpression
{
    private IExpression first;
    private IExpression second;
    private RelationalOperation operation;

    public RelationalExpression(IExpression first, IExpression second, RelationalOperation operation)
    {
        this.first = first;
        this.second = second;
        this.operation = operation;
    }

    private boolean evaluate(IntValue value1, IntValue value2, RelationalOperation operation)
    {
        switch (operation)
        {
            case LESS_THAN: return value1.getValue() < value2.getValue();
            case LESS_THAN_OR_EQUAL: return value1.getValue() <= value2.getValue();
            case EQUAL: return value1.getValue().equals(value2.getValue());
            case NOT_EQUAL: return !value1.getValue().equals(value2.getValue());
            case GREATER_THAN: return value1.getValue() > value2.getValue();
            case GREATER_THAN_OR_EQUAL: return value1.getValue() >= value2.getValue();
            default: throw new UnsupportedOperationException();
        }
    }

    @Override
    public IValue eval(ProgramState state) throws SomeException
    {
        IValue eval1 = first.eval(state);
        if (eval1.getType().equals(ValueType.IntType))
        {
            IValue eval2 = second.eval(state);
            if (eval2.getType().equals(ValueType.IntType))
            {
                IntValue value1 = (IntValue) eval1;
                IntValue value2 = (IntValue) eval2;

                return new BoolValue(evaluate(value1, value2, operation));
            }
            else
                throw new SomeException("Second operand is not an int");
        }
        else
            throw new SomeException("First operand is not an int");
    }

    @Override
    public Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException
    {
        Type type = first.typecheck(typeEnv);
        Type type2 = second.typecheck(typeEnv);

        if (type.equals(ValueType.IntType))
        {
            if (type2.equals(ValueType.IntType))
            {
                return ValueType.BoolType;
            }
            else
                throw new SomeException("Second operand is not an integer");
        }
        else
            throw new SomeException("First operand not int");
    }

    @Override
    public String toString()
    {
        return first + " " + operation.getSymbol() + " " +  second;
    }
}
