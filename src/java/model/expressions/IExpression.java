package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.types.Type;
import model.values.IValue;

public interface IExpression {
    IValue eval(ProgramState state) throws SomeException;
    Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException;
}
