package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.operations.BooleanOperation;
import model.types.Type;
import model.types.ValueType;
import model.values.BoolValue;
import model.values.IValue;

public class LogicalExpression implements IExpression {
    private IExpression first;
    private IExpression second;
    private BooleanOperation operation;

    public LogicalExpression(IExpression first,
                             IExpression second,
                             BooleanOperation operation)
    {
        this.first = first;
        this.second = second;
        this.operation = operation;
    }

    @Override
    public IValue eval(ProgramState state) throws SomeException
    {
        IValue v1, v2;
        v1 = first.eval(state);
        if (v1.getType().equals(ValueType.BoolType))
        {
            v2 = second.eval(state);
            if (v2.getType().equals(ValueType.BoolType))
            {
                BoolValue b1 = (BoolValue)v1;
                BoolValue b2 = (BoolValue)v2;
                boolean a, b;
                a = b1.getValue();
                b = b2.getValue();
                switch(operation) {
                    case AND: return new BoolValue(a && b);
                    case OR: return new BoolValue(a || b);
                    case XOR: return new BoolValue(a ^ b);
                    default: throw new UnsupportedOperationException();
                }
            }
            else
                throw new SomeException("Second operand is not a boolean");
        }
        else
            throw new SomeException("First operand is not a boolean");
    }

    @Override
    public Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException
    {
        Type type = first.typecheck(typeEnv);
        Type type1 = second.typecheck(typeEnv);
        if (type.equals(ValueType.BoolType))
        {
            if (type1.equals(ValueType.BoolType))
            {
                return ValueType.BoolType;
            }
            else
                throw new SomeException("Second operand not bool");
        }
        throw new SomeException("First operand not bool");
    }

    @Override
    public String toString()
    {
        return first + " " + operation.toString() + " " + second;
    }
}
