package model.expressions;

import model.ProgramState;
import model.collections.dictionary.IDictionary;
import model.exceptions.SomeException;
import model.expressions.operations.ArithmeticOperation;
import model.types.Type;
import model.types.ValueType;
import model.values.IValue;
import model.values.IntValue;

public class ArithmeticExpression implements IExpression {
    private IExpression first;
    private IExpression second;
    private ArithmeticOperation operation;

    public ArithmeticExpression(IExpression first,
                                IExpression second,
                                ArithmeticOperation operation) {
        this.first = first;
        this.second = second;
        this.operation = operation;
    }

    @Override
    public IValue eval(ProgramState state) throws SomeException {
        IValue v1, v2;
        v1 = first.eval(state);
        if (v1.getType().equals(ValueType.IntType))
        {
            v2 = second.eval(state);
            if (v2.getType().equals(ValueType.IntType))
            {
                IntValue i1 = (IntValue)v1;
                IntValue i2 = (IntValue)v2;
                int n1, n2;
                n1 = i1.getValue();
                n2 = i2.getValue();
                switch (operation) {
                    case ADDITION: return new IntValue(n1 + n2);
                    case SUBTRACTION: return new IntValue(n1 - n2);
                    case MULTIPLICATION: return new IntValue(n1 * n2);
                    case DIVISION:
                        if (n2 == 0)
                            throw new SomeException("Division by zero");
                        else return new IntValue(n1 / n2);
                    default:
                        throw new UnsupportedOperationException();
                }
            }
            else
                throw new SomeException("Second operand not and integer!");
        }
        else
            throw new SomeException("First operand not an integer");
    }

    @Override
    public Type typecheck(IDictionary<String, Type> typeEnv) throws SomeException
    {
        Type type1 = first.typecheck(typeEnv);
        Type type2 = second.typecheck(typeEnv);

        if (type1.equals(ValueType.IntType))
        {
            if (type2.equals(ValueType.IntType))
            {
                return ValueType.IntType;
            }
            else
                throw new SomeException("Second operand is not an integer");
        }
        else
            throw new SomeException("First operand not int");
    }

    @Override
    public String toString()
    {
        return first + " " + operation.toString() + " " + second;
    }
}
