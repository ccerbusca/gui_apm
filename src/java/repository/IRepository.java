package repository;

import model.ProgramState;
import model.exceptions.SomeException;

import java.util.List;

public interface IRepository<T> {
    void add(T e);
    List<ProgramState> getAll();
    void setProgramList(List<T> other);
    void logPrgStateExec(T elem) throws SomeException;
}
