import model.ProgramState;
import model.collections.dictionary.Dictionary;
import model.collections.dictionary.Heap;
import model.collections.dictionary.IDictionary;
import model.collections.dictionary.IHeap;
import model.expressions.ValueExpression;
import model.statements.AllocateStatement;
import model.types.RefType;
import model.values.IValue;
import model.values.IntValue;
import model.values.RefValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static model.types.ValueType.IntType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AllocateStatementTest
{
    @Mock
    private ProgramState programState;
    private IHeap<IValue> heap;
    private IDictionary<String, IValue> symTable;

    @BeforeEach
    private void setup()
    {
        heap = new Heap();
        symTable = new Dictionary<>();
        when(programState.getHeap()).thenReturn(heap);
        when(programState.getSymTable()).thenReturn(symTable);
    }

    @Test
    public void testAllocate()
    {
        symTable.put("test", (new RefType(IntType)).getDefaultValue());
        AllocateStatement allocateStatement = new AllocateStatement(
                "test", new ValueExpression(new IntValue(1)));
        allocateStatement.execute(programState);
        assertThat(heap.lookup(1)).isEqualToComparingFieldByField(new IntValue(1));
        RefValue value = (RefValue) symTable.lookup("test");
        assertThat(value).isEqualToComparingFieldByField(new RefValue(1, IntType));
    }

}
