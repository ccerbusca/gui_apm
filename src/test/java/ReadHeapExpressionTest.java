import model.ProgramState;
import model.collections.dictionary.Heap;
import model.collections.dictionary.IHeap;
import model.expressions.ReadHeapExpression;
import model.expressions.ValueExpression;
import model.types.ValueType;
import model.values.IValue;
import model.values.IntValue;
import model.values.RefValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReadHeapExpressionTest
{
    @Mock
    private ProgramState state;
    @Mock
    private IHeap<IValue> valueIHeap;

    @BeforeEach
    void setup()
    {
        when(state.getHeap()).thenReturn(valueIHeap);
        when(valueIHeap.lookup(any())).thenReturn(new IntValue(10));
        when(valueIHeap.contains(1)).thenReturn(true);
    }

    @Test
    void testReadHeap()
    {
        ReadHeapExpression expression = new ReadHeapExpression(
                new ValueExpression(new RefValue(1, ValueType.IntType)));
        assertThat(expression.eval(state)).isEqualToComparingFieldByField(new IntValue(10));
    }
}
