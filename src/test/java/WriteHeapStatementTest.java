import model.ProgramState;
import model.collections.dictionary.Dictionary;
import model.collections.dictionary.Heap;
import model.collections.dictionary.IDictionary;
import model.collections.dictionary.IHeap;
import model.expressions.ValueExpression;
import model.statements.WriteHeapStatement;
import model.values.IValue;
import model.values.IntValue;
import model.values.RefValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static model.types.ValueType.IntType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WriteHeapStatementTest
{
    @Mock
    private ProgramState state;
    private IHeap<IValue> heap;
    private IDictionary<String, IValue> symTable;

    @BeforeEach
    private void setup()
    {
        heap = new Heap();
        symTable = new Dictionary<>();
        when(state.getHeap()).thenReturn(heap);
        when(state.getSymTable()).thenReturn(symTable);
    }

    @Test
    public void testWriteHeap()
    {
        Integer address = heap.put(new IntValue(-1));
        symTable.put("test", new RefValue(address, IntType));
        WriteHeapStatement statement = new WriteHeapStatement(
                "test", new ValueExpression(new IntValue(1)));
        statement.execute(state);
        assertThat(heap.lookup(address)).isEqualToComparingFieldByField(new IntValue(1));
    }
}
